package controller;

import java.util.ArrayList;

import model.data_structures.IntegersBag;
import model.logic.IntegersBagOperations;

public class Controller {

	private static IntegersBagOperations model = new IntegersBagOperations();
	
	
	public static IntegersBag createBag(ArrayList<Integer> values){
         return new IntegersBag(values);		
	}
	
	
	public static double getMean(IntegersBag bag){
		return model.computeMean(bag);
	}
	
	public static Integer getMax(IntegersBag bag){
		return model.getMax(bag);
	}
	
	public static Integer getMin(IntegersBag bag)
	{
		return model.getMin(bag);
	}
	
	public static double getStandardDeviation(IntegersBag bag)
	{
		return model.getStandardDeviation(bag);
	}
	
	public static int computeNumbersUnderMean(IntegersBag bag)
	{
		return model.computeNumbersUnderMean(bag);
	}
}
