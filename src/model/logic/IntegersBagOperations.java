package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag)
	{
		int Min = Integer.MAX_VALUE;
		int value;
		if (bag!=null)
		{
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				value = iter.next();
				if (value<Min)
					Min = value;
			}
		}
		return Min;
	}
	
	public double getStandardDeviation(IntegersBag bag)
	{
		double mean = computeMean(bag);
		int samples = -1;
		double sum = 0;
		if (bag!=null)
		{
			Iterator <Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				samples ++;
				sum += Math.pow(iter.next()-mean, 2);				
			}
		}
		return Math.sqrt(sum/samples);	
	}
	
	public int computeNumbersUnderMean(IntegersBag bag)
	{
		int numbers=0;
		double mean = computeMean(bag);
		if (bag!=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				if (iter.next()<mean)
					numbers++;
			}
		}
		return numbers;
	}
}
